#!/usr/bin/env bash

echo "this script only for Debain based , root"
echo "-----------------------------------------------------------"
#sudo su //uncomment this to run as root
echo "-----------------------------------------------------------"
echo "update && upgrade"
apt-get update && apt-get upgrade
echo "done"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"
echo "install apache2"
apt-get install apache2
echo "done"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"
echo "install mysql server"
apt-get install mysql-server
mysql_secure_installation
echo "done"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"
echo "install php && phpmyadmin"
apt-get install php libapache2-mod-php php-mcrypt php-mysql php-zip phpmyadmin apache2-utils
echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
echo "done"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"
echo "restarting apache2"
service apache2 restart
echo "done"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"
echo "test :)"
echo "-----------------------------------------------------------"
echo "-----------------------------------------------------------"

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
